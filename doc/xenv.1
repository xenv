.\" xenv - expand environment variables in input files
.\" Copyright (C) 2021-2023 Sergey Poznyakoff
.\"
.\" Xenv is free software; you can redistribute it and/or modify it
.\" under the terms of the GNU General Public License as published by the
.\" Free Software Foundation; either version 3 of the License, or (at your
.\" option) any later version.
.\"
.\" Xenv is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License along
.\" with xenv. If not, see <http://www.gnu.org/licenses/>.
.TH XENV 1 "February 23, 2023" "XENV" "General Commands Manual"
.SH NAME
xenv \- expand shell variables and commands in input files
.SH SYNOPSIS
\fBxenv\fR\
 [\fB\-hmnrsuvx?\fR]\
 [\fB\-e \fINAME\fR]\
 [\fB\-D \fINAME\fR[\fB=\fIVALUE\fR]]\
 [\fB\-p \fICOMMAND\fR]\
 [\fB\-t \fISECONDS\fR]\
 [\fB\-U \fINAME\fR]\
 [\fB\-W \fR[\fBno-\fR]\fIFEATURE\fR]\
 [\fIFILE\fR...]
.SH DESCRIPTION
Reads input from \fIFILE\fRs (or the standard input, if none are
supplied) and prints it on the standard output, expanding references to
environment variables with their actual values and substituting shell
commands with the output they produce.
.SS Variable expansion
A \fIvariable reference\fR is one of the following constructs:
.TP
\fB$\fINAME\fR
Expands to the value of the variable \fINAME\fR, or to an empty
string, if it is unset.
.TP
\fB${\fINAME\fB}\fR
Same as above.
.PP
The following forms are conditional expressions, that cause \fBxenv\fR
to test for a variable that is unset or null and act accordingly.
Omitting the colon results in a test only for a variable that is unset.
.TP
.BI ${ variable :- word }
.BR "Use Default Values" .
If \fIvariable\fR is unset or null, the expansion of \fIword\fR is substituted.
Otherwise, the value of \fIvariable\fR is substituted.
.TP
.BI ${ variable := word }
.BR "Assign Default Values" .
If \fIvariable\fR is unset or null, the expansion of \fIword\fR is
assigned to \fIvariable\fR.  The value of \fIvariable\fR is then substituted.
.TP
.BI ${ variable :? word }
.BR "Display Error if Null or Unset" .
If \fIvariable\fR is null or unset, the expansion of \fIword\fR (or a
message to that effect if word is not present) is output to standard error.
Otherwise, the value of \fIvariable\fR is substituted.
.TP
.BI ${ variable :+ word }
.BR "Use Alternate Value" .
If \fIvariable\fR is null or unset, nothing is substituted, otherwise the
expansion of \fIword\fR is substituted.
.TP
.BI ${ variable :| word1 | word2 }
.BR "Ternary operator" .
Unless \fIvariable\fR is null or unset, substitutes the expansion of
\fIword1\fR, otherwise the expansion of \fIword2\fR.
.PP
The above notation is consistent with the POSIX shell, except for the
.BI ${ variable :| word1 | word2 }
form, which is an extension of \fBxenv\fR.
.PP
In the above expansion forms the \fIword\fR part can contain variable
expansions, command substitutions, single and double-quoted parts.
The latter two are handled exactly as in Bourne shell: the quotes are
removed, the text between double quotes is subject to variable
expansion and command substitution, whereas the test between single
quotes is not.  Within double quoted part, a backslash can be used to
escape the indirection character (\fB$\fR), double and single quote
character and itself.
.SS Command substitution
A construct
.BI $( command )
is replaced with the output of \fIcommand\fR.  \fBXenv\fR performs
substitution by running \fB$SHELL -c \fIcommand\fR and replacing the
construct with the standard output of the command, with any trailing
newlines removed.  If the \fBSHELL\fR variable is not set, the default
\fB/bin/sh\fR is assumed.
.PP
\fIcommand\fR is passed to the shell verbatim.  This means, in
particular, that variable references in the command are expanded by
shell, rather than by \fBxenv\fR and, as a consequence, that the
options
.B \-u
and
.B \-r
don't work for command substitutions.  The sequences
\fB$[\fR...\fB]\fR and \fB${*\fR...\fB*}\fR lose their special meaning
as well.
.PP
Nested command substitutions are processed by the shell.
.PP
The \fB\-t \fIN\fR option sets the maximum time a command substitution is
allowed to run.  If the command runs longer than \fIN\fR seconds, it
is terminated and a diagnostic message to that effect is printed on
the standard error.
.PP
Command substitution can be disabled using the \fB\-Wno\-command\fR option.
.SS Comments
Comments are multiline.  They are introduced with the characters
.B ${*
and extend up to the nearest pair of characters
.BR *} .
Comments are removed from the input.
.PP
Comments can be disabled using the \fB\-Wno\-comment\fR option.
.SS Quoting and escaping
To deprive the \fB$\fR character of its special meaning, precede it
with a backslash.  For example:
.PP
.EX
\\${NAME}
.EE
.PP
Backslash followed by another backslash is replaced by single
backslash.  Backslash followed by any other character is reproduced
verbatim.
.PP
To turn off this feature, use the
.B \-Wno\-escape
option.
.PP
To reproduce a portion of text verbatim, enclose it in \fB$[\fR and
\fB]\fR.  This has the same effect as the \fB$$verbatim\fR directive
described below, except that it allows the verbatim portion to appear
within a line.
.PP
Newlines and balanced square brackets are allowed within the \fB$[\fR ...
\fB]\fR construct.
.PP
Special meaning of the \fB$[\fR...\fB]\fR construct can be disabled by
the \fB\-Wno\-quote\fR option.
.SS Environment meta-variable
Environment meta-variable makes variable indirections more visible.
Use it if your input text can contain considerable number of
indirection characters (\fB$\fRs) that can be falsely identified as
variable references.  The meta-variable is defined using the \fB-e\fR
option:
.sp
.EX
xenv -e ENV
.EE
.sp
Once you define it, variable reference syntax becomes \fB$ENV{\fINAME\fB}\fR,
instead of just \fB$\fINAME\fR.  Tests for variable that is unset or null are
modified accordingly.  Thus, e.g. \fB$ENV{\fINAME\fB:-text}\fR substitutes
"text" if the variable \fINAME\fR is unset or null.  The
constructs for command substitution, verbatim quotations and comments become
\fB$ENV(...)\fR, \fB$ENV[...]\fR, and \fB$ENV{* ... *}\fR, correspondingly.
.PP
Any sequence of characters can be used to name the meta-variable,
provided that it is a valid variable name.
.SS Preprocessor directives
The two \fB$\fR characters appearing at the beginning of line (with
optional preceding whitespace) introduce special preprocessor
directives.
.PP
Preprocessor directives can be disabled using the
\fB\-Wno\-directive\fR option.
.TP
.B Inclusion
The constructs
.sp
.EX
\fB$$source \fIFILE\fR
\fB$$include \fIFILE\fR
.EE
.sp
cause \fIFILE\fR to be read and processed at the current point.  When the
end of the file is reached, input is resumed from the previous
input file.
.sp
Unless \fIFILE\fR is an absolute file name, it will be searched in the
\fIinclude search path\fR.  This path, initially empty, is initialized
by \fB\-I\fR command line options.  The argument of each \fB\-I\fR
option is appended to the include search path.
.sp
If the \fIFILE\fR cannot be processed, \fBxenv\fR reports an error and
exits.  The exit code is 66, if the file does not exist and cannot be
found in the include file path, 77, if the program is denied
permission to open it, and 72 if another error occurred.  If an
attempt of recursive inclusion is detected, the program reports the
fact and exits with the code 69.
.sp
The construct
.sp
.EX
\fB$$sinclude \fIFILE\fR
.EE
.sp
is similar to \fB$$include\fR, except that if the file is not found,
it silently ignores the fact.
.TP
.B Diversion
Diversions are a way of directing output to a temporary storage and
inserting it (\fIundiverting\fR) into the main output stream again,
at a later time.  Temporary storage used for each diversion is
identified by a unique identifier assigned to it when a
diversion is declared.
.sp
A diversion is started by the \fB$$divert\fR directive:
.sp
.EX
\fB$$divert \fINAME\fR
.EE
.sp
The \fINAME\fR gives the identifier of a temporary storage to divert
output to.  If this diversion does not exist, it will be created.  If
it already exists, output will be appended to it.  If \fINAME\fR is
omitted, initial output file is restored.
.sp
The output is appended to that diversion until the next \fB$$divert\fR
directive or end of input, whichever happens first.  The collected
text can be inserted anyplace by using the \fB$$undivert\fR directive:
.sp
.EX
\fB$$undivert \fINAME\fR
.EE
.sp
Undiverting the collected text does not discard it: you can use
\fB$$undivert\fR multiple times to insert the same text again and
again.  To actually destroy the diversion and free the resources
associated with it, use this directive:
.sp
.EX
\fB$$dropdivert \fINAME\fR
.EE
.sp
After dropping a diversion, attempts to undivert from it result
in error.  You can however recreate the dropped diversion from
scratch using the \fB$$divert\fR directive.
.TP
.B Verbatim block
The following construct introduces verbatim text block:
.sp
.EX
\fB$$verbatim\fR
\fITEXT\fR
\fB$$end\fR
.EE
.sp
It expands to \fITEXT\fR unchanged.  To insert verbatim text in a
line, use the \fB$[\fR ... \fB]\fR construct (see \fBQuoting and
escaping\fR, above).
.PP
The following \fIconditional directives\fR expand to a given fragment
of text depending on whether an environment variable is defined.
.TP
.B Expand if defined
The construct
.sp
.EX
\fB$$ifdef\fR \fINAME\fR
\fITEXT1\fR
\fB$$else\fR
\fITEXT2\fR
\fB$$endif\fR
.EE
.sp
is replaced with the expansion of \fITEXT1\fR if the
environment variable \fINAME\fR is defined (including if it has a null
value) and to \fITEXT2\fR otherwise.
.sp
If the construct begins with \fB$$ifndef\fR, the sense is inverted.
.TP
.B Expand if set
The construct
.sp
.EX
\fB$$ifset\fR \fINAME\fR
\fITEXT1\fR
\fB$$else\fR
\fITEXT2\fR
\fB$$endif\fR
.EE
.sp
is replaced with the expansion of \fITEXT1\fR if the
environment variable \fINAME\fR is set and has a non-null
value and to \fITEXT2\fR otherwise.
.sp
If the construct begins with \fB$$ifnset\fR, the sense is inverted.
.TP
.B Expand if true
The following construct substitutes \fITEXT1\fR if the variable
\fINAME\fR evaluates to 1 (\fIboolean true\fR) and substitutes \fITEXT2\fR
otherwise:
.sp
.EX
\fB$$iftrue\fR \fINAME\fR
\fITEXT1\fR
\fB$$else\fR
\fITEXT2\fR
\fB$$endif\fR
.EE
.TP
.B Expand if false
The following construct substitutes \fITEXT1\fR if the variable
\fINAME\fR evaluates to 0 (\fIboolean false\fR) and substitutes \fITEXT2\fR
otherwise:
.sp
.EX
\fB$$iffalse\fR \fINAME\fR
\fITEXT1\fR
\fB$$else\fR
\fITEXT2\fR
\fB$$endif\fR
.EE
.PP
In the context of \fB$$iftrue\fR and \fBiffalse\fR, a variable that is
unset is considered to evaluate to false.  Textual values for false
and true are configurable via the \fB\-Wbooleans\fR option.  If
\fINAME\fR does not evaluate to a valid boolean value, an error is
reported and both constructs substitute \fITEXT2\fR.
.PP
In the conditional constructs above, the \fB$$else\fR part is
optional.
.PP
Optional whitespace is allowed between the beginning of the line and the
\fB$$\fR marker, as well as between it and the keyword.  This allows
for indenting the nested constructs in a more natural way, e.g.:
.PP
.EX
$$ifdef LOG_HOST
$$ ifdef LOG_PORT
  logger $LOG_HOST:$LOG_PORT;
$$ else
  logger $LOG_HOST
$$ endif
$$endif
.EE
.SS Diagnostics
.TP
.BI $$error " TEXT"
This directive causes
.N xenv
to report a fatal error at the current location.  Rest of
the line following the whitespace after
.B $$error
is used as the error message.  Notice, that there's no need to quote
the message.
.TP
.BI $$warning " TEXT"
Similar to
.BR $$error ,
but reports a warning and does not alter exit status.
.TP
.BI $$exit " N"
Causes immediate exit.  Status code \fIN\fR is returned to the shell.
If used without argument, exit code is determined by usual rules.
.SS Loops
Two looping directives are provided.
.PP
.TP
.B Foreach loop
.sp
.EX
\fB$$loop \fIVAR\fR \fIARGS...\fR
\fIBODY\fR
\fB$$end\fR
.EE
.sp
Current value of the environment variable \fIVAR\fR is stashed away.
The arguments (\fIARGS...\fR) are subject to all normal expansions.
Words obtained as a result of expansions are assigned to \fIVAR\fR
sequentially.  After each assignment, \fIBODY\fR is expanded.  When
all expanded words have been iterated over, initial value of \fIVAR\fR
is restored.
.sp
For example, the following text:
.sp
.EX
$$loop X A B C
This is $X
$$end
.EE
.sp
expands to:
.sp
.EX
This is A
This is B
This is C
.EE
.TP
.B For loop
.sp
.EX
\fB$$range \fIVAR\fR \fISTART\fR \fISTOP\fR [\fIINCR\fR]
\fIBODY\fR
\fB$$end\fR
.EE
.sp
Current value of the environment variable \fIVAR\fR is stashed away.
and it is assigned the expansion of \fISTART\fR, which must be an
integer value.  \fIBODY\fR is expanded and \fIVAR\fR is
incremented by the expansion of \fINCR\fR until its value becomes
greater than the expansion of \fISTOP\fR.
.sp
When iteration is over, original value of \fIVAR\fR is restored.
.sp
If \fIINCR\fR is omitted, it defaults to +1 if \fISTOP\fR is greater
than \fISTART\fR and to -1 otherwise.
.sp
For example
.sp
.EX
$$range X 1 4
Number $X
$$end
.EE
.sp
produces the following expansion:
.sp
.EX
Number 1
Number 2
Number 3
Number 4
.EE
.SS Evaluation blocks
An eval block introduces a portion of text that will be expanded twice, thus
allowing for creation of variable names and expanding them on the fly.
.PP
The
.B $$eval
directive marks start of the text that should be evaluated after
expansion.  The end is marked with
.BR $$end .
The text between
.B $$eval
and
.B $$end
is read and expanded, and then processed again.  It is that second
pass that creates output.
.PP
As an example, the following loop considers environment variables
.B VAR_0
through
.B VAR_7
and prints those of them that are defined:
.PP
.EX
$$range I 0 7
$$eval
\\$\\$ifset VAR_$I
Expand VAR_$I = \\$VAR_$I;
\\$\\$endif
$$end
$$end
.EE
.PP
Notice escaping of
.B $
signs that should be retained until second pass.
.SS Setting and unsetting variables
.TP
\fB$$set \fINAME\fR
Sets the variable \fINAME\fR to empty string.
.TP
\fB$$set \fINAME\fR \fB"\fISTRING\fB"\fR
Sets the variable \fINAME\fR to \fISTRING\fR.  \fISTRING\fR can occupy
multiple lines and is subject to variable expansion and command
substitution.
.TP
\fB$$set \fINAME\fR \fB'\fISTRING\fB'\fR
Sets the variable \fINAME\fR to \fISTRING\fR.  \fISTRING\fR can occupy
multiple lines.  Neither variable expansion nor command substitution
occurs in \fISTRING\fR.
.TP
\fB$$unset \fINAME\fR
Unsets the variable \fINAME\fR.
.SH OPTIONS
.TP
\fB\-e\fR \fINAME\fR
Use \fINAME\fR as the environment meta-variable.  See the section
.BR "Environment meta-variable" ,
above.
.TP
\fB\-h\fR or \fB\-?\fR
Print a short command line summary and exit.
.TP
.B \-m
Pipe output to \fBm4\fR.  If \fB\-s\fR option is also given, pass it
to \fBm4\fR as well.  See also \fB\-p\fR.
.TP
.B \-n
Dry-run mode.  Process the input files without producing any output.
Report any errors.  Useful together with the \fB\-u\fR option to
discover undefined variables.
.TP
\fB\-p \fICOMMAND\fR
Pipe output to \fICOMMAND\fR.
.TP
.B \-r
Retain references to undefined variables in output.  By default, an
undefined variable expands to an empty string.  This option instructs
\fBxenv\fR to reproduce the variable reference verbatim in the output.
Naturally, this affects only \fB$\fIX\fR and \fB${\fIX\fB}\fR
references.
.TP
.B \-s
Generate synchronization directives, i.e. lines of the form
\fB#line \fINUM\fR "\fIFILE\fR"\fR, which mean that the following line
originated at line \fINUM\fR in file \fIFILE\fR.
.br
Synchronization directives are emitted when variable or preprocessor
directive expansion causes removal or insertion of one or more lines
to the output.  Each synchronization directive occupies a single line
by itself.  If a synchronization discrepancy occurs in the middle of
an output line, emission of the synchronization directive is delayed
until the next newline that does not occur in the middle of a quoted
string (both single and double quotes are understood).
.TP
\fB\-t \fISECONDS\fR
Sets maximum execution time for a command substitution.
.TP
.B \-u
Treat unset variables as an error.
.TP
\fB\-D \fINAME\fR[\fB=\fIVALUE\fR]
Define environment variable \fINAME\fR to the \fIVALUE\fR, or an
empty string, if it is not given.
.TP
\fB\-U \fINAME\fR
Undefine the environment variable \fINAME\fR.
.TP
\fB\-v\fR
Print program version and exit.
.TP
\fB\-W \fR[\fBno\-\fR]\fIFEATURE\fR
Disable (if prefixed with \fBno\-\fR) or enable specific \fBxenv\fR
feature.  Valid \fIFEATURE\fR names are:
.RS
.TP
\fBbooleans=\fIT\fB/\fIF\fR[,\fIT\fB/\fIF\fR]
Defines values that are considered booleans by \fB$$iftrue\fR and
\fB$$iffalse\fR.  Each \fIT\fR is registered as a boolean
true, and each \fIF\fR as a boolean false.
.TP
.B command
Controls command expansion.
.TP
.B comment
Controls \fB${* \fR... \fB*}\fR comments.
.TP
.B escape
Controls the use of backslash as escape character.
.TP
.B directive
Controls preprocessor directives: \fB$$set\fR, \fB$$ifset\fR and the
like.
.TP
.B quote
Controls the use of quote (\fB$[\fR ... \fB]\fR) constructs.
.TP
.B minimal
Enables minimal mode.  Equivalent to:
.nh
.na
\fB\-Wno\-command\
 \-Wno\-comment\
 \-Wno\-directive\
 \-Wno\-quote\
 \-Wno\-escape\
 \-e ENV\fR
.ad
.hy
.RE
.TP
.B \-x
Enable debugging output.
.SH EXIT CODES
.TP
.B "0"
Successful termination.
.TP
.B "64"
Usage error.
.TP
.B "65"
Reference to undefined variable encountered and the \fB\-u\fR option
is specified.
.TP
.B "66"
Source file does not exist.
.TP
.B "69"
Recursive inclusion has been prevented.
.TP
.B "70"
Internal program error.  This probably indicates a bug.  Please,
report it.
.TP
.B "71"
System error: file cannot be opened, fork failed, etc.
.TP
.B "77"
Permission denied to open the source file.
.SH BUGS
The \fB\-r\fR and \fB\-u\fR options have no effect over command
substitutions.
.SH SEE ALSO
.BR dbe (1),
.BR m4env (1).
.SH COPYRIGHT
Copyright \(co 2021-2022 Sergey Poznyakoff <gray@gnu.org>
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.\\-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:
