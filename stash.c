/* This file is part of xenv.
   Copyright (C) 2021-2023 Sergey Poznyakoff

   Xenv is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Xenv is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with xenv. If not, see <http://www.gnu.org/licenses/>. */
#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <sysexits.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include "xenv.h"

size_t max_stash_files;
size_t max_stash_memory;

struct stashcookie {
	struct stashcookie *prev, *next;
	struct iobuf {
		char *base;
		size_t size;
		size_t len;
		size_t pos;
	} iob;
	char *filename;
	int fd;
	off64_t file_off;
	FILE *owner;
};

static struct stashcookie *cookie_head, *cookie_tail;
static size_t cur_stash_files;

static void
stash_link(struct stashcookie *dp)
{
	dp->next = NULL;
	dp->prev = cookie_tail;
	if (cookie_tail)
		cookie_tail->next = dp;
	else
		cookie_head = dp;
	cookie_tail = dp;
}

static void
stash_unlink(struct stashcookie *dp)
{
	if (dp->prev)
		dp->prev->next = dp->next;
	else
		cookie_head = dp->next;
	if (dp->next)
		dp->next->prev = dp->prev;
	else
		cookie_tail = dp->prev;
	dp->next = dp->prev = NULL;
}

static void
stash_promote(struct stashcookie *dp)
{
	if (dp != cookie_tail) {
		stash_unlink(dp);
		stash_link(dp);
	}
}

int
stash_freefd(void)
{
	if (!cookie_head || cookie_head == cookie_tail || cookie_head->fd == -1)
		return -1;
	cookie_head->file_off = lseek(cookie_head->fd, 0, SEEK_CUR);
	if (cookie_head->file_off == -1)
		return -1;
	close(cookie_head->fd);
	cookie_head->fd = -1;
	cur_stash_files--;
	return 0;
}

static inline int
stash_fd(struct stashcookie *dp)
{
	stash_promote(dp);
	if (dp->fd == -1) {
		if (cur_stash_files == max_stash_files)
			stash_freefd();
		if (!dp->filename) {
#define TEMPLATE_NAME "/xenv.XXXXXX"
			char const *tmpdir = getenv("TMP");

			if (!tmpdir)
				tmpdir = "/tmp";
			else if (*tmpdir == 0)
				tmpdir = ".";
			dp->filename = xmalloc(strlen(tmpdir) + sizeof(TEMPLATE_NAME));
			do {
				strcat(strcpy(dp->filename, tmpdir), TEMPLATE_NAME);
				dp->fd = mkstemp(dp->filename);
			} while (dp->fd == -1 && errno == EMFILE && stash_freefd() == 0);
			if (dp->fd == -1)
				xenv_error(EX_OSERR, errno, "can't create temporary file");
		} else {
			do {
				dp->fd = open(dp->filename, O_RDWR);
			} while (dp->fd == -1 && errno == EMFILE && stash_freefd() == 0);
			if (dp->fd == -1)
				xenv_error(EX_OSERR, errno, "can't open temporary file");
			if (lseek(cookie_head->fd, cookie_head->file_off, SEEK_SET) == -1)
				xenv_error(EX_OSERR, errno, "can't seek in temporary file");
		}
		cur_stash_files++;
	}
	return dp->fd;
}

static ssize_t
stash_write(void *cookie, const char *buffer, size_t size)
{
	struct stashcookie *dp = cookie;
	size_t total = 0;

	if (dp->iob.len < dp->iob.size) {
		size_t n = dp->iob.size - dp->iob.len;
		if (n > size)
			n = size;
		memcpy(dp->iob.base + dp->iob.len, buffer, n);
		dp->iob.len += n;
		total += n;
	}

	if (total < size) {
		ssize_t n = write(stash_fd(dp), buffer + total, size - total);
		if (n >= 0)
			total += n;
		else if (total == 0) {
			xenv_error(EX_OSERR, errno, "error writing to a temporary file");
			return -1;
		}
	}
	return total;
}

static ssize_t
stash_read(void *cookie, char *buffer, size_t size)
{
	struct stashcookie *dp = cookie;
	size_t total = 0;

	if (dp->iob.pos < dp->iob.len) {
		size_t n = dp->iob.len - dp->iob.pos;
		if (n > size)
			n = size;
		memcpy(buffer, dp->iob.base + dp->iob.pos, n);
		dp->iob.pos += n;
		total += n;
	}
	if (size > total && dp->iob.len == dp->iob.size) {
		ssize_t n = read(stash_fd(dp), buffer + total, size - total);
		if (n >= 0)
			total += n;
		else if (total == 0) {
			xenv_error(EX_OSERR, errno, "error reading from temporary file");
			return -1;
		}
	}
	return total;
}

static int
stash_seek(void *cookie, off64_t *position, int whence)
{
	struct stashcookie *dp = cookie;
	off64_t pos = *position;

	switch (whence) {
	case SEEK_SET:
		pos = *position;
		break;

	case SEEK_CUR:
		if (dp->filename) {
			if ((pos = lseek(stash_fd(dp), 0, SEEK_CUR)) == -1)
				return -1;
			pos += dp->iob.pos;
		} else {
			pos = dp->iob.pos + *position;
		}
		if (pos < 0) {
			errno = EPIPE;
			return -1;
		}
		break;

	case SEEK_END:
		if (dp->filename) {
			if ((pos = lseek(stash_fd(dp), 0, SEEK_END)) == -1)
				return -1;
			pos += dp->iob.pos;
		} else {
			pos = dp->iob.pos + *position;
		}
		pos += dp->iob.len;
		break;
	}

	if (pos < dp->iob.size) {
		dp->iob.pos = pos;
		if (dp->fd == -1)
			dp->file_off = 0;
		else if (lseek(dp->fd, 0, SEEK_SET) == -1)
			return -1;
	} else {
		if (lseek(stash_fd(dp), pos - dp->iob.size, SEEK_SET) == -1)
			return -1;
		dp->iob.pos = dp->iob.len = dp->iob.size;
	}
	*position = pos;
	return 0;
}

static int
stash_close(void *cookie)
{
	struct stashcookie *dp = cookie;
	stash_unlink(dp);
	free(dp->iob.base);
	if (dp->fd != -1) {
		unlink(dp->filename);
		free(dp->filename);
		close(dp->fd);
		cur_stash_files--;
	}
	free(dp);
	return 0;
}

FILE *
stash_open(void)
{
	struct stashcookie *dp;
	static cookie_io_functions_t iof = {
		.read = stash_read,
		.write = stash_write,
		.seek = stash_seek,
		.close = stash_close
	};
	FILE *fp;

	dp = xmalloc(sizeof(*dp));
	dp->iob.base = xmalloc(max_stash_memory);
	dp->iob.size = max_stash_memory;
	dp->iob.len = 0;
	dp->iob.pos = 0;
	dp->filename = NULL;
	dp->fd = -1;
	dp->prev = dp->next = NULL;

	fp = fopencookie(dp, "w+", iof);
	if (fp) {
		dp->owner = fp;
		stash_link(dp);
	} else
		stash_close(dp);
	return fp;
}

void
stash_done(void)
{
	while (cookie_head) {
		fclose(cookie_head->owner);
	}
}

void
stash_init(void)
{
	struct rlimit rlim;

	if (getrlimit(RLIMIT_NOFILE, &rlim))
		xenv_error(EX_OSERR, errno, "can't get open file limit");
	max_stash_files = rlim.rlim_cur - rlim.rlim_cur / 3;
	max_stash_memory = sysconf(_SC_PAGESIZE);
	atexit(stash_done);
}
