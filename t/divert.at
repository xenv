# This file is part of xenv                         -*- autotest -*-
# Copyright (C) 2021-2023 Sergey Poznyakoff
#
# Xenv is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 3 of the License, or (at your
# option) any later version.
#
# Xenv is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with xenv. If not, see <http://www.gnu.org/licenses/>. */

AT_SETUP([diversions])
AT_KEYWORDS([diversions divert undivert])

AT_CHECK([
AT_DATA([input],
[begin text
$$divert A
this text is diverted first
$$divert B
this text is diverted after it
$$divert
main output restored
$$undivert B
some other text
$$undivert A
end text
])
xenv input
],
[0],
[begin text
main output restored
this text is diverted after it
some other text
this text is diverted first
end text
])

AT_CHECK([
AT_DATA([input],
[$$divert A
et sequitur
$$divert
Incipiat
$$ifset X
$$ undivert A
$$endif
Finit
])
unset X
xenv input
echo ======
X=1 xenv input
],
[0],
[Incipiat
Finit
======
Incipiat
et sequitur
Finit
])

AT_CHECK([
AT_DATA([input],
[Incipit hic
$$divert A
et hic sequitur
$$divert
$$range X 1 3
$$undivert A
$$end
atque finit
])
xenv -s input
],
[0],
[#line 1 "input"
Incipit hic
#line 3 "input"
et hic sequitur
#line 3 "input"
et hic sequitur
#line 3 "input"
et hic sequitur
#line 8 "input"
atque finit
])

AT_CHECK([
AT_DATA([input],
[Incipit hic
$$divert A
et hic sequitur
$$divert
hic finit
$$undivert A
$$dropdivert A
$$undivert A
])
xenv input
],
[0],
[Incipit hic
hic finit
et hic sequitur
],
[input:8.1-13: no such diversion: A
])

AT_CHECK([
AT_DATA([input],
[$$divert A
et sic sequitur
$$divert
Incipit hic
$$undivert A
atque finit
$$divert A
iterum atque iterum
$$divert
Iterum scribit
$$undivert A
verba scribens
])
xenv -s input
],
[0],
[#line 4 "input"
Incipit hic
#line 2 "input"
et sic sequitur
#line 6 "input"
atque finit
#line 10 "input"
Iterum scribit
#line 2 "input"
et sic sequitur
#line 8 "input"
iterum atque iterum
#line 12 "input"
verba scribens
])

AT_CLEANUP
