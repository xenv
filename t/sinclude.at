# This file is part of xenv                         -*- autotest -*-
# Copyright (C) 2021-2023 Sergey Poznyakoff
#
# Xenv is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 3 of the License, or (at your
# option) any later version.
#
# Xenv is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with xenv. If not, see <http://www.gnu.org/licenses/>. */

AT_SETUP([$$sinclude])

AT_DATA([1.inc],
[cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
$$sinclude ./2.inc
])

AT_DATA([input],
[Duis aute irure dolor in reprehenderit in voluptate velit esse
$$sinclude ./1.inc
anim id est laborum.
])

AT_CHECK([xenv input],
[0],
[Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
anim id est laborum.
])

AT_CHECK([xenv -s input],
[0],
[#line 1 "input"
Duis aute irure dolor in reprehenderit in voluptate velit esse
#line 1 "./1.inc"
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
#line 3 "input"
anim id est laborum.
])

AT_CHECK([
AT_DATA([input],
[start
$$range "text"
finish
])
xenv input
],
[0],
[start
$$range "text"
finish
],
[input:2.1-7: malformed $$range directive
])

AT_CLEANUP
