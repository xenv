# This file is part of xenv                         -*- autotest -*-
# Copyright (C) 2021-2023 Sergey Poznyakoff
#
# Xenv is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 3 of the License, or (at your
# option) any later version.
#
# Xenv is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with xenv. If not, see <http://www.gnu.org/licenses/>. */

AT_SETUP([Display Custom Error if Null or Unset])

AT_DATA([input],
[Ut enim ad minim $ENV{V1?Should have set V1}, quis nostrud exercitation ullamco
$ENV{V2:?Should have set V2 to a non-empty value} nisi ut aliquip ex ea commodo consequat.
])

AT_CHECK([
V1=veniam
V2=laboris
export V1 V2
xenv -e ENV input
],
[0],
[Ut enim ad minim veniam, quis nostrud exercitation ullamco
laboris nisi ut aliquip ex ea commodo consequat.
])

AT_CHECK([
V1=
V2=laboris
export V1 V2
xenv -e ENV input
],
[0],
[Ut enim ad minim , quis nostrud exercitation ullamco
laboris nisi ut aliquip ex ea commodo consequat.
])

AT_CHECK([
unset V1
V2=laboris
export V2
xenv -e ENV input
],
[0],
[Ut enim ad minim , quis nostrud exercitation ullamco
laboris nisi ut aliquip ex ea commodo consequat.
],
[input:1.18-44: Should have set V1
])

AT_CHECK([
unset V1 V2
xenv -e ENV input
],
[0],
[Ut enim ad minim , quis nostrud exercitation ullamco
 nisi ut aliquip ex ea commodo consequat.
],
[input:1.18-44: Should have set V1
input:2.1-49: Should have set V2 to a non-empty value
])

AT_CHECK([
unset V1
V2=
export V2
xenv -e ENV input
],
[0],
[Ut enim ad minim , quis nostrud exercitation ullamco
 nisi ut aliquip ex ea commodo consequat.
],
[input:1.18-44: Should have set V1
input:2.1-49: Should have set V2 to a non-empty value
])

AT_CHECK([
AT_DATA([input],
[$ENV{X:?text with 'single: $ENV{Y}' and "double: \"$ENV{Y}\"" quotes}
])
unset X
Y=string xenv -e ENV input
],
[0],
[
],
[input:1.1-69: text with single: $ENV{Y} and double: "string" quotes
])

AT_CLEANUP
