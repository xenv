/* This file is part of xenv.
   Copyright (C) 2021-2023 Sergey Poznyakoff

   Xenv is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Xenv is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with xenv. If not, see <http://www.gnu.org/licenses/>. */

void xenv_error(int status, int errnum, char const *fmt, ...);
void enomem(void);
void *xmalloc(size_t size);
void *xcalloc(size_t nmemb, size_t size);
void *xrealloc(void *ptr, size_t size);
void *x2nrealloc(void *p, size_t *pn, size_t s);
char *xstrdup(const char *s);

extern size_t max_stash_files;
extern size_t max_stash_memory;
FILE *stash_open(void);
void stash_init(void);
FILE *sink_open(void);
