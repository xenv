/* This file is part of xenv.
   Copyright (C) 2021-2023 Sergey Poznyakoff

   Xenv is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Xenv is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with xenv. If not, see <http://www.gnu.org/licenses/>. */

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include "xenv.h"

struct sink {
	off64_t pointer;
};

static ssize_t
sink_write(void *cookie, const char *buffer, size_t size)
{
	struct sink *sink = cookie;
	sink->pointer += size;
	return size;
}

static int
sink_seek(void *cookie, off64_t *position, int whence)
{
	struct sink *sink = cookie;
	off64_t n;
	switch (whence) {
	case SEEK_SET:
		n = *position;
		break;

	case SEEK_CUR:
	case SEEK_END:
		n = sink->pointer + *position;
		break;
	}

	if (n < 0) {
		errno = EPIPE;
		return -1;
	}
	*position = n;
	return 0;
}

static int
sink_close(void *cookie)
{
	free(cookie);
	return 0;
}

FILE *
sink_open(void)
{
	struct sink *sink;
	static cookie_io_functions_t iof = {
		.read = NULL,
		.write = sink_write,
		.seek = sink_seek,
		.close = sink_close
	};
	if ((sink = malloc(sizeof(*sink))) == NULL)
		return NULL;
	sink->pointer = 0;
	return fopencookie(sink, "w+", iof);
}
